<?php
return [
    'controllers' => [
        'path' => [
            app_path('Http'.DIRECTORY_SEPARATOR .'Controllers'),
        ],
        'valid_actions' => [
            'index',
            'create',
            'store',
            'show',
            'edit',
            'update',
            'destroy',
            'search'
        ]
    ],
];
