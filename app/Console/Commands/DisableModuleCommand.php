<?php

namespace App\Console\Commands;

use Nwidart\Modules\Commands\DisableCommand;
use Symfony\Component\Console\Input\InputArgument;

class DisableModuleCommand extends DisableCommand
{
    /**
     * The name and name of the console command.
     *
     * @var string
     */
    protected $name = 'module:disable';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Disable the specified module. Modified for Digital Artisan';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $module = $this->laravel['modules']->findOrFail($this->argument('module'));

        if (env('APP_DEBUG') == false) {
            if ($module->get('required') === true) {
                return $this->comment("Module [{$module}] cannot be disabled.");
            }
        }

        if ($module->enabled()) {
            $module->disable();
            $this->info("Module [{$module}] disabled successful.");
        } else {
            $this->comment("Module [{$module}] has already disabled.");
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['module', InputArgument::REQUIRED, 'Module name.'],
        ];
    }
}
