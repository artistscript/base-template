<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Facades\Modules\Manager\Manager as ManagerFacades;

class Installer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install the application';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->checkRequirements();

        $envFile = base_path('.env');

       if (!file_exists($envFile)) {
           copy ('.env.example', $envFile);
       } else {
           if (!$this->confirm("Configuration file already exist. Do you want to continue?")) {
               exit;
           }
       }

        $lines = file($envFile);

        $data['APP_NAME'] = $this->ask("Enter Application Name", "'Base Template by Digital Artisan'");
        $data['APP_URL'] = $this->ask("Enter Application url (Example: http://testing.dev)", "http://localhost");
        $data['DB_CONNECTION'] = $this->choice("What is your database provider?", ['mysql', 'pgsql'], 'mysql');
        $data['DB_HOST'] = $this->ask("Enter DB Host", "127.0.0.1");
        if ($data['DB_CONNECTION'] == 'mysql') {
            $data['DB_PORT'] = $this->ask("Enter DB Port", "3306");
        } else {
            $data['DB_PORT'] = $this->ask("Enter DB Port", "5432");
        }

        $data['DB_DATABASE'] = $this->ask("Enter Database Name", "testing");
        $data['DB_USERNAME'] = $this->ask("Enter Database Username", "root");
        $data['DB_PASSWORD'] = $this->secret("Enter Database Password", "secret");

        $data['CACHE_DRIVER'] = $this->choice("What is your cache driver?", ['redis', 'memcached'], 'redis');

        $data['APP_DEBUG'] = true;

        $this->testDatabase($data);
        $this->info("Saving configuration..");

        // grep pattern to get non empty line
        $pattern = '/([^\=]*)\=[^\n]*/';

        // new line data
        $newLines = [];
        foreach ($lines as $line) {
            preg_match($pattern, $line, $matches);

            if (!count($matches)) {
                array_push($newLines, $line);
                continue;
            }

            if (!key_exists(trim($matches[1]), $data)) {
                array_push($newLines, $line);
                continue;
            }

            $line = trim($matches[1]) . "={$data[trim($matches[1])]}\n";
            array_push($newLines, $line);
        }

        $newContent = implode('', $newLines);
        file_put_contents($envFile, $newContent);

        $this->setCache($data['CACHE_DRIVER']);

        $this->info("Config saved!");
        $this->info("Creating new key..");
        $this->call("key:generate");

        // reload config for database
        $this->changeDBConfig($data, 'migration');

        $this->info("Running Migration..");
        \Artisan::call("migrate:fresh");
        $this->info("Database Migrated! Seeding Data..");

        // reload config for database
        $this->changeDBConfig($data, 'seeder');
        $this->call('db:seed');
        $this->info("Default data seeded!");
        $this->info("Installing Module Manager..");
        ManagerFacades::module('Manager')->install();
        $this->info("Application Ready! You can now login to $data[APP_URL] with username: admin@digitalartisan.co.id and password: admin123");
    }

    private function testDatabase($data)
    {
        $this->info("Connecting to database with provided information..");
        if ($data['DB_CONNECTION'] == 'mysql') {
            $link = mysqli_connect(
                $data['DB_HOST'],
                $data['DB_USERNAME'],
                $data['DB_PASSWORD']
            ) or die("Unable to connect to MySQL @" . $data['DB_HOST']);

            mysqli_select_db($link, $data['DB_DATABASE']) or die("Could Not Open Database!");
        } else {
            $link = pg_connect(
                "host=$data[DB_HOST] dbname=$data[DB_DATABASE] user=$data[DB_USERNAME] password=$data[DB_PASSWORD]"
            ) or die("Unable to connect to PgSQL @" . $data['DB_HOST']);
        }

        $this->info("Connection Success!");
    }

    private function changeDBConfig($data, $state)
    {
        if ($state == 'seeder') {
            // first, change default connection ONLY FOR SEEDER
            $confPath = 'database.connections.' . $data['DB_CONNECTION'];
            config()->set($confPath. '.host', $data['DB_HOST']);
            config()->set($confPath. '.port', $data['DB_PORT']);
            config()->set($confPath. '.database', $data['DB_DATABASE']);
            config()->set($confPath. '.username', $data['DB_USERNAME']);
            config()->set($confPath. '.password', $data['DB_PASSWORD']);

            \Artisan::call('config:clear');
            \Artisan::call('cache:clear');
            \DB::disconnect(env("DB_DATABASE"));
            \DB::connection($data['DB_CONNECTION'])->reconnect();
            \DB::reconnect(env("DB_CONNECTION"));
        } elseif ($state == 'migration') {
            // next, create temporary connection to handle migrations
            config()->set('database.default', 'temp');

            if ($data['DB_CONNECTION'] == 'mysql') {
                // mysql
                config()->set('database.connections.temp', [
                'driver' => 'mysql',
                'host' => $data['DB_HOST'],
                'port' => $data['DB_PORT'],
                'database' => $data['DB_DATABASE'],
                'username' => $data['DB_USERNAME'],
                'password' => $data['DB_PASSWORD'],
                'unix_socket' => '',
                'charset' => 'utf8mb4',
                'collation' => 'utf8mb4_unicode_ci',
                'prefix' => '',
                'strict' => true,
                'engine' => null,
            ]);
            } else {
                // pgsql
                config()->set('database.connections.temp', [
                'driver' => 'pgsql',
                'host' => $data['DB_HOST'],
                'port' => $data['DB_PORT'],
                'database' => $data['DB_DATABASE'],
                'username' => $data['DB_USERNAME'],
                'password' => $data['DB_PASSWORD'],
                'charset' => 'utf8',
                'prefix' => '',
                'schema' => 'public',
                'sslmode' => 'prefer',
            ]);
            }

            \Artisan::call('config:clear');
            \Artisan::call('cache:clear');
            \DB::disconnect(env('DB_DATABASE'));

            \DB::setDefaultConnection('temp');
            \DB::reconnect('temp');
        }
    }

    private function setCache($cacheType)
    {
        return config()->set('cache.default', $cacheType);
    }

    private function checkRequirements() : array
    {
        if (phpversion() >= '7.0') {
            $requirements['PHP Version'] = true;
            $this->info("PHP Version: " . phpversion() . " ✓");
        } else {
            $this->error("PHP Version: " . phpversion() . " ✘");
        }

        $required_extensions = [
            'openssl',
            'pdo_mysql',
            'pdo_pgsql',
            'pdo_sqlite',
            'mbstring',
            'tokenizer',
            'zip',
            'xmlrpc',
            'xml',
            'json',
            'ctype',
            'bcmath'
        ];

        foreach ($required_extensions as $key => $value) {
            if (in_array($value, get_loaded_extensions())) {
                $requirements[$value] = true;
                $this->info("PHP Extension: " . $value . " ✓");
            } else {
                $requirements[$value] = false;
                $this->error("PHP Extension: " . $value . " ✘");
            }
        };

        foreach ($requirements as $key => $value) {
            if ($requirements[$key] == false) {
                $this->alert("There are unsatisfied requirements in your server config");
                if (!$this->confirm("Do you want to continue?")) {
                    exit;
                }
            }
        }

        return $requirements;
    }
}
