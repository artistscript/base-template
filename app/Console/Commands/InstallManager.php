<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Facades\Modules\Manager\Manager as ManagerFacades;

class InstallManager extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'manager:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install module manager';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (ManagerFacades::module('Manager')->install()) {
            $this->comment("Module Manager is Installed!");
        }
    }
}
