<?php

namespace Helpers;

use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use App\Models\MenuItem;

class PermissionGenerator
{
    public $classDirectories;
    public $validActions;

    private function getClassDirs()
    {
        $this->classDirectories = config('system.system.controllers.path');
        return $this->classDirectories;
    }

    public function get()
    {
        $this->getClassDirs();

        $permission = [];
        foreach ($this->classDirectories as $directory) {
            $p = $this->getControllers($directory);
            $permission = array_merge($permission, $p);
        }

        return $permission;
    }

    private function getControllers($directory)
    {
        $classes = [];
        foreach (\File::allFiles($directory) as $file) {
            // Skip Controller.php
            if ($file->getFileName() == 'Controller.php') {
                continue;
            }

            $class = $this->getClass($file->getFileName());
            array_push($classes, $class);
        }
        return $classes;
    }

    private function getClass($file)
    {
        return explode('.php', $file)[0];
    }

    public function getPermissions()
    {
        $classes = $this->get();
        $validActions = $this->getValidActions();
        $permissions = [];

        foreach ($classes as $class) {
            foreach ($validActions as $action) {
                $permission = $class . '@' . $action;
                array_push($permissions, $permission);
            }
        }

        return $permissions;
    }

    private function getValidActions()
    {
        $this->validActions = config('system.system.controllers.valid_actions');
        return $this->validActions;
    }

    public function generatePermission()
    {
        $permissions = $this->getPermissions();
        $result = [];

        foreach ($permissions as $permission) {
            $p = Permission::firstOrCreate([
                'name' => $permission
            ]);

            array_push($result, $p);
        }
        return $result;
    }

    public function generateAdminRole($syncWithFirstUser = false)
    {
        $role = Role::firstOrCreate([
            'name' => 'System Administrators',
        ]);

        foreach ($this->generatePermission() as $permission) {
            $role->givePermissionTo($permission);
        }

        if ($syncWithFirstUser) {
            $user = User::first();
            $user->assignRole($role);
        }
    }

    public function allowSidemenuForAdmin()
    {
        $role = Role::firstOrCreate([
            'name' => 'System Administrators',
        ]);

        $role->menuitems()->sync(MenuItem::pluck('id'));

        return true;
    }
}
