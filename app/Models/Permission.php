<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Spatie\Activitylog\Traits\LogsActivity;

class Permission extends \Spatie\Permission\Models\Permission
{
    use CrudTrait;
    use LogsActivity;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $connection = 'mysql';
    protected $table = 'permissions';
    protected $primaryKey = 'id';
    protected $fillable = ['name', 'guard_name', 'updated_at', 'created_at'];
    protected static $logAttributes = ['name', 'guard_name'];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    protected static function boot()
    {
        parent::boot();
        if (empty($model->guard_name)) {
            static::creating(function ($model) {
                try {
                    $model->guard_name = 'web';
                } catch (Exception $e) {
                    abort(500, $e->getMessage());
                }
            });
        }
    }

    public function save(array $options = [])
    {
        cache()->tags('menuTree')->flush();
        return parent::save($options);
    }

    public function delete()
    {
        cache()->tags('menuTree')->flush();
        return parent::delete();
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
