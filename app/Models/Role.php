<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Spatie\Activitylog\Traits\LogsActivity;

class Role extends \Spatie\Permission\Models\Role
{
    use CrudTrait, LogsActivity;

    protected $connection = 'mysql';
    protected $table = 'roles';
    protected $primaryKey = 'id';
    protected $fillable = ['name', 'guard_name', 'updated_at', 'created_at'];

    // activity logger setting
    protected static $logAttributes = ['name', 'guard_name'];

    /**
    *
    */
    public function save(array $options = [])
    {
        cache()->tags('menuTree')->flush();
        return parent::save($options);
    }

    public function delete()
    {
        cache()->tags('menuTree')->flush();
        return parent::delete();
    }

    /**
    *
    */
    public function menuitems()
    {
        return $this->belongsToMany('App\Models\MenuItem', 'menu_item_role', 'role_id', 'menu_item_id');
    }
}
