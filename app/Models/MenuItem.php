<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class MenuItem extends Model
{
    use CrudTrait, LogsActivity;

    protected $connection = 'mysql';
    protected $table = 'menu_items';
    protected $fillable = ['name', 'type', 'link', 'page_id', 'parent_id', 'icon', 'admin_route'];
    protected static $logAttributes = ['name', 'type', 'link', 'page_id', 'parent_id', 'icon', 'admin_route'];
    protected $casts = [];
    protected $dates = ['created_at', 'updated_at'];

    public function getLogNameToUse(string $eventName = ''): string
    {
        return 'Model';
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\MenuItem', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\Models\MenuItem', 'parent_id');
    }

    public function page()
    {
        return $this->belongsTo('Backpack\PageManager\app\Models\Page', 'page_id');
    }

    public function save(array $options = [])
    {
        cache()->tags('menuTree')->flush();
        cache()->forget('MenuItem');
        return parent::save($options);
    }

    public function delete()
    {
        cache()->tags('menuTree')->flush();
        cache()->forget('MenuItem');
        return parent::delete();
    }

    public function roles()
    {
        return $this->belongsToMany('App\Models\Role', 'menu_item_role', 'menu_item_id', 'role_id');
    }

    /**
     * Get all menu items, in a hierarchical collection.
     * Only supports 2 levels of indentation.
     */
    public static function getTree()
    {
        $menu = self::orderBy('lft')->get();

        if ($menu->count()) {
            foreach ($menu as $k => $menu_item) {
                $menu_item->children = collect([]);

                foreach ($menu as $i => $menu_subitem) {
                    if ($menu_subitem->parent_id == $menu_item->id) {
                        $menu_item->children->push($menu_subitem);

                        // remove the subitem for the first level
                        $menu = $menu->reject(function ($item) use ($menu_subitem) {
                            return $item->id == $menu_subitem->id;
                        });
                    }
                }
            }
        }

        return $menu;
    }

    public function url()
    {
        switch ($this->type) {
            case 'external_link':
                return $this->link;
                break;

            case 'internal_link':
                return is_null($this->link) ? '#' : url($this->link);
                break;

            default: //page_link
                if ($this->page) {
                    return url($this->page->slug);
                }
                break;
        }
    }
}
