<?php
/**
 * Created by PhpStorm.
 * User: Adli Ikhwanur Ifkar
 * Date: 2017-08-16
 * Time: 14:42
 */

namespace App\Repositories;

use App\Models\MenuItem;
use Carbon\Carbon;

class MenuItems
{
    protected $model = '';

    public function __construct()
    {
        if (empty($this->model)) {
            $this->model = new MenuItem();
        }
    }

    public function tree()
    {
        if (cache()->tags(['menuTree'])->has(session()->getId()) == false) {
            cache()
                ->tags(['menuTree'])
                ->add(session()->getId(), $this->getTree(0), Carbon::now()->addMinutes(5));
        }
        return cache()->tags(['menuTree'])->get(session()->getId());
    }

    public function all()
    {
        return cache()->remember('MenuItem', 2, function () {
            return MenuItem::all();
        });
    }

    public function getParent()
    {
        return $this->all()->pluck('name', 'id');
    }

    public function findByParentId($id)
    {
        return $this->all()->where('id', $id)->first();
    }

    private function getTree()
    {
        $menu = MenuItem::orderBy('lft')
            ->whereIn('id', $this->getUserNav())
            ->get();

        if ($menu->count()) {
            foreach ($menu as $k => $menu_item) {
                $menu_item->children = collect([]);

                foreach ($menu as $i => $menu_subitem) {
                    if ($menu_subitem->parent_id == $menu_item->id) {
                        $menu_item->children->push($menu_subitem);

                        // remove the subitem for the first level
                        $menu = $menu->reject(function ($item) use ($menu_subitem) {
                            return $item->id == $menu_subitem->id;
                        });
                    }
                }
            }
        }

        return $menu;
    }

    private function getUserNav()
    {
        $nav = [];
        foreach (auth()->user()->roles as $role) {
            foreach ($role->menuitems()->pluck('menu_item_id') as $mi) {
                if (!in_array($mi, $nav)) {
                    array_push($nav, $mi);
                }
            }
        }
        return $nav;
    }

    public function findByName($name)
    {
        return MenuItem::whereName($name)->first();
    }

    public function flushTree()
    {
        return cache()->tags('menuTree')->flush();
    }
}
