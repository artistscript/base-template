<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use Backpack\CRUD\app\Http\Controllers\CrudController;
// VALIDATION: change the requests to match your own file names if you need form validation
use Backpack\CRUD\app\Http\Requests\CrudRequest as StoreRequest;
use Backpack\CRUD\app\Http\Requests\CrudRequest as UpdateRequest;

use App\Repositories\MenuItems;

class MenuItemCrudController extends CrudController
{
    public function __construct(MenuItems $menuItems)
    {
        parent::__construct();

        $this->crud->setModel("App\Models\MenuItem");
        $this->crud->setRoute(config('backpack.base.route_prefix').'/menu-item');
        $this->crud->setEntityNameStrings('menu item', 'menu items');

        $this->crud->allowAccess('reorder');
        $this->crud->enableReorder('name', 10);

        $this->crud->addColumns([
            [
                'name' => 'name',
                'label' => 'Label',
            ],
            [
                'label' => 'Parent',
                'type' => 'repositories',
                'name' => 'parent_id',
                'attribute' => 'name',
                'repositories' => '\App\Repositories\MenuItems',
                'function' => 'findByParentId',
            ]
        ]);

        $this->crud->addFields([
            [
                'name' => 'type',
                'type' => 'hidden',
                'value' => 'link',
            ],
            [
                'name' => 'name',
                'label' => 'Label',
            ],
            // [
            //     'name' => 'type',
            //     'label' => 'Type',
            //     'type' => 'page_or_link',
            //     'page_model' => '\Backpack\PageManager\app\Models\Page',
            // ],
            [
                'label' => 'URL',
                'type' => 'text',
                'name' => 'link',
            ],
            [
                'label' => "Icon",
                'name' => 'icon',
                'type' => 'icon_picker',
                'iconset' => 'fontawesome'
            ],
            [
                'label' => 'Parent',
                'type' => 'select2_from_array',
                'name' => 'parent_id',
                'entity' => 'parent',
                'attribute' => 'name',
                'options' => $menuItems->getParent(),
                'allows_null' => true,
            ],
            // [
            //     'name' => 'admin_route',
            //     'label' => 'Admin Only?',
            //     'type' => 'checkbox'
            // ],
        ]);
    }

    public function store(StoreRequest $request)
    {
        return parent::storeCrud($request);
    }

    public function update(UpdateRequest $request)
    {
        return parent::updateCrud($request);
    }
}
