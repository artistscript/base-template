<?php

namespace App\Http\Controllers\Locale;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LocaleController extends Controller
{

    public $locale;
    public $existingLocale;

    public function set(Request $request)
    {
        foreach (config('backpack.crud.locales') as $key => $value) {
            if ($request->locale == $key) {
                session(['locale' => $key]);
            }
        }

        // save selected locale if user is logged in
        if (auth()->user()) {
            $user = User::find(auth()->user()->uuid);

            // save language preference to user config
            $config = $user->config;
            $config['lang'] = $request->locale;
            $user->config = $config;
            $user->save();
        }

        return redirect()->back();
    }
}
