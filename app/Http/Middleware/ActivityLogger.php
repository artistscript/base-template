<?php

namespace App\Http\Middleware;

use Closure;

class ActivityLogger
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // if (config('system.base.log_web_access')) {
        //     activity('access')->log('Web / Api Access');
        // }
        activity('access')->log('Web / Api Access');
        return $next($request);
    }
}
