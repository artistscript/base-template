<?php

namespace App\Http\Middleware;

use Closure;
use App;

class Locale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // mirror content of config('backpack.crud.locales') to config('app.locales')
        config([
            'app.locales' => config('backpack.crud.locales')
        ]);

        // Determine if 'locale' exist in session
        if (!empty(session('locale'))) {
            $locale = $this->checkIfLocaleExists(session('locale'));
        } else {
            // check if user is authenticated
            if (auth()->user()) {
                if (isset(auth()->user()->config['lang'])) {
                    session(['locale' => auth()->user()->config['lang']]);
                    $locale = $this->checkIfLocaleExists(session('locale'));
                } else {
                    $locale = config('app.locale');$this->checkIfLocaleExists();
                }
            } else {
                session(['locale' => config('app.locale')]);
                $locale = config('app.locale');
            }
        }

        // set the locale
        App::setlocale($locale);
        return $next($request);
    }

    /**
     * @param $locale
     * @return string
     */
    private function checkIfLocaleExists($locale) : string
    {
        foreach (config('app.locales') as $key => $value) {
            if (session('locale') == $key) {
                if ($key == $locale) {
                    return $locale;
                } else {
                    // returning default locale if lang not exists
                    return config('app.fallback_locale');
                }
            }
        }
    }
}
