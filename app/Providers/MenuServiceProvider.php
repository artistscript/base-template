<?php

namespace App\Providers;

use App\Repositories\MenuItems;
use Illuminate\Support\ServiceProvider;

class MenuServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     * @param MenuItems $menuItems
     */
    public function boot(MenuItems $menuItems)
    {
        view()->composer('backpack::inc.sidebar_content', function ($view) use ($menuItems) {
            $data['navigations'] = $menuItems->tree();
            // $data['navigations_admin'] = $menuItems->adminTree();
            $view->with($data);
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
