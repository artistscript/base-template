/*
*
* Backpack Crud / List
*
*/

jQuery(function($){

    'use strict';

    $('#crudTable_filter input').unbind();

    $('#crudTable_filter input').keyup(function(e){
        if (e.keyCode == 13) {
            $('#crudTable').dataTable().fnFilter( this.value );
        } else {
            return false;
        }
    });
});
