<li>
    <a href="{{ url(config('backpack.base.route_prefix') . '/' . $m->link) }}">
        <i class="fa {{ $m->icon }}"></i>
        <span>{{ $m->name }}</span>
    </a>
</li>