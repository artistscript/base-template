<li class="treeview">
    <a href="#">
        <i class="fa {{ $mi->icon }}"></i>
        <span>{{ $mi->name }}</span>
        <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
        @foreach ($mi->children as $i => $child)
            @if($child->children->count() > 0)
                @include('vendor.backpack.base.inc._sidebar._withchild', ['mi'=>$child])
            @else
                @include('vendor.backpack.base.inc._sidebar._nochild', ['m'=>$child])
            @endif
        @endforeach
    </ul>
</li>