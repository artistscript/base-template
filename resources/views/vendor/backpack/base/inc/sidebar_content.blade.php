<!-- search form (Optional) -->
<form action="#" method="get" class="sidebar-form">
  <div class="input-group">
    <input type="text" name="q" class="form-control search-menu-box" placeholder="Search Menu...">
    <span class="input-group-btn">
          <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
          </button>
        </span>
  </div>
</form>

<!-- /.search form -->

<ul class="sidebar-menu" id="sidebar-menu">
    <li class="header"><span>Menu</span></li>
    <li><a href="{{ backpack_url('dashboard') }}"><i class="fa fa-tachometer-alt"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li>

    @if ($navigations->count())
        @foreach ($navigations as $k => $navigation)
            @if ($navigation->children->count() > 0)
                @include('backpack::inc._sidebar._withchild', ['mi'=>$navigation])
            @else
                @include('backpack::inc._sidebar._nochild', ['m'=>$navigation])
            @endif
        @endforeach
    @endif
</ul>

@push('after_scripts')
<script>
$(document).ready(function(){
    $(".search-menu-box").on('input', function() {
        var filter = $(this).val();
        $(".sidebar-menu li").each(function(){
            if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                // console.log($(this));
                $(this).hide();
            } else {
                // console.log($(this).parent());
                $(this).show();
                $(this).parentsUntil(".treeview").openMenu();
            }
        });
    });
});

$.fn.openMenu = function () {
        var className = $(this).attr('class');
    if(className == "treeview"){
        $(this).addClass("active");
    }else if(className == "treeview-menu" ){
        $(this).addClass("menu-open");
        $(this).css({ display: "block" });
    }
};

$.fn.closeMenu = function () {
    var className = $(this).attr('class');
    var count = $(this).length;
    if(count > 1){
        $.each($(this), function( key, element ) {
            className = $(element).attr('class');
            if(className == "treeview active"){
                $(element).removeClass("active");
            }else if(className == "treeview-menu menu-open" ){
                $(element).removeClass("menu-open");
                $(element).css({ display: "none" });
            }
        });
    }else{
        if(className == "treeview active"){
            $(this).removeClass("active");
        }else if(className == "treeview-menu menu-open" ){
            $(this).removeClass("menu-open");
            $(this).css({ display: "none" });
        }
    }
};
</script>
@endpush
