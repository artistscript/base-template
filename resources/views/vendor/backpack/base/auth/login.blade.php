<!DOCTYPE html>
<html lang="en">
<head>
    <title>Login V15</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
    <link rel="icon" type="image/png" href="/lgn-rsrc/images/icons/favicon.ico"/>
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/lgn-rsrc/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/lgn-rsrc/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/lgn-rsrc/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/lgn-rsrc/vendor/animate/animate.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/lgn-rsrc/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/lgn-rsrc/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/lgn-rsrc/vendor/select2/select2.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/lgn-rsrc/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/lgn-rsrc/css/util.css">
    <link rel="stylesheet" type="text/css" href="/lgn-rsrc/css/main.css">
<!--===============================================================================================-->
</head>
<body>

    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100">
                <div class="login100-form-title" style="background-image: url(/lgn-rsrc/images/bg-01.jpg);">
                    <span class="login100-form-title-1">
                        Sign In
                    </span>
                </div>

                <form class="form-horizontal login100-form validate-form" role="form" method="POST" action="/login">
                    {!! csrf_field() !!}

                    <div class="wrap-input100 validate-input m-b-26 form-group{{ $errors->has($username) ? ' has-error' : '' }}" data-validate="Email is required">
                        <span class="label-input100">{{ config('backpack.base.authentication_column_name') }}</span>
                        <input class="input100" type="text" name="{{ $username }}" value="{{ old($username) }}" placeholder="Enter username">
                        <span class="focus-input100"></span>
                        @if ($errors->has($username))
                            <span class="help-block"><small>{{ $errors->first($username) }}</small></span>
                        @endif
                    </div>

                    <div class="wrap-input100 validate-input m-b-18" data-validate = "Password is required">
                        <span class="label-input100">{{ trans('backpack::base.password') }}</span>
                        <input class="input100" type="password" name="password" placeholder="Enter password">
                        <span class="focus-input100"></span>
                        @if ($errors->has('password'))
                            <span class="help-block"><small>{{ $errors->first('password') }}</small></span>
                        @endif
                    </div>

                    <div class="flex-sb-m w-full p-b-30">
                        <div class="contact100-form-checkbox">
                            <input class="input-checkbox100" id="ckb1" type="checkbox" name="remember">
                            <label class="label-checkbox100" for="ckb1">
                                {{ trans('backpack::base.remember_me') }}
                            </label>
                        </div>

<!--                         <div>
                            <a href="/lgn-rsrc/#" class="txt1">
                                Forgot Password?
                            </a>
                        </div> -->

                    </div>

                    <div class="container-login100-form-btn">
                        <button class="login100-form-btn">
                            Login
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

<!--===============================================================================================-->
    <script src="/lgn-rsrc/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
    <script src="/lgn-rsrc/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
    <script src="/lgn-rsrc/vendor/bootstrap/js/popper.js"></script>
    <script src="/lgn-rsrc/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
    <script src="/lgn-rsrc/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
    <script src="/lgn-rsrc/vendor/daterangepicker/moment.min.js"></script>
    <script src="/lgn-rsrc/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
    <script src="/lgn-rsrc/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
    <script src="/lgn-rsrc/js/main.js"></script>

</body>
</html>