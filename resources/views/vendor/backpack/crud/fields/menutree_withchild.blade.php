<div class="row">
    <div class="col-sm-4
    @if($level > 0)
        col-sm-offset-{{$level}}
    @endif
    ">
        <label>
            <input type="checkbox"
                   name="{{ $field['name'] }}[]"
                   value="{{ $connected_entity_entry->id }}"

                   @if( ( old( $field["name"] ) && in_array($connected_entity_entry->id, old( $field["name"])) ) || (isset($field['value']) && in_array($connected_entity_entry->id, $field['value']->pluck('id', 'id')->toArray())))
                   checked = "checked"
                    @endif > {!! $connected_entity_entry->{$field['attribute']} !!}
        </label>
    </div>
</div>

@foreach ($connected_entity_entry->children as $i => $child)
    @php($level = 1)
    @if($child->children->count() > 0)
        @include('crud::fields.menutree_withchild', [
                    'connected_entity_entry'=>$child,
                    'field' => $field,
                    'level' => $level
                ])
    @else
        @include('crud::fields.menutree_nochild', [
                    'connected_entity_entry'=>$child,
                    'field' => $field,
                    'level' => $level
                ])
    @endif
@endforeach