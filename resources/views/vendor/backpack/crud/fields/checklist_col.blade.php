<!-- select2 -->
<div @include('crud::inc.field_wrapper_attributes') >
    <label>{!! $field['label'] !!}</label>
    @include('crud::inc.field_translatable_icon')


    <div class="row">
        @foreach ($field['model']::all() as $connected_entity_entry)
            <div class="col-sm-{{ isset($field['number_columns']) ? intval(12/$field['number_columns']) : '4'}}">
                <div class="checkbox">
                    <label>
                        <input type="checkbox"
                               name="{{ $field['name'] }}[]"
                               value="{{ $connected_entity_entry->id }}"

                               @if( ( old( $field["name"] ) && in_array($connected_entity_entry->id, old( $field["name"])) ) || (isset($field['value']) && in_array($connected_entity_entry->id, $field['value']->pluck('id', 'id')->toArray())))
                               checked = "checked"
                                @endif > {!! $connected_entity_entry->{$field['attribute']} !!}
                    </label>
                </div>
            </div>
        @endforeach
    </div>

    {{-- HINT --}}
    @if (isset($field['hint']))
        <p class="help-block">{!! $field['hint'] !!}</p>
    @endif
</div>
