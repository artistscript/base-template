<div @include('crud::inc.field_wrapper_attributes') >
    <label>{!! $field['label'] !!}</label>
@include('crud::inc.field_translatable_icon')
    @foreach ($field['model']::getTree() as $k => $connected_entity_entry)
        @php($level = 0)
        @if ($connected_entity_entry->children->count() > 0)
            @include('crud::fields.menutree_withchild', [
                'connected_entity_entry'=>$connected_entity_entry,
                'field' => $field,
                'level' => $level
            ])
        @else
            @include('crud::fields.menutree_nochild', [
                'connected_entity_entry'=>$connected_entity_entry,
                'field' => $field,
                'level' => $level
            ])
        @endif
    @endforeach
</div>