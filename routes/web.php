<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Route::get('setLang', 'Locale\LocaleController@set');

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', config('backpack.base.middleware_key', 'admin')],
    'namespace'  => '\App\Http\Controllers\Admin',
], function () {
    // Storage Route
    Route::get('storage/{type}/{file}', function ($type, $file) {
        $mime = Storage::disk('dm_mails')->getMimeType("$type" . DIRECTORY_SEPARATOR . "$file");
        $file = Storage::disk('dm_mails')->get("$type" . DIRECTORY_SEPARATOR . "$file");
        return response($file, 200)->header('Content-Type', $mime);
    });

    CRUD::resource('menu-item', 'MenuItemCrudController');
    CRUD::resource('user', 'UserCrudController');
    CRUD::resource('role', 'RoleCrudController');
    CRUD::resource('permission', 'PermissionCrudController');
});
