<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterActivityLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('activity_log', function (Blueprint $table) {
            $table->string('subject_id')->nullable()->change();
            $table->string('causer_id')->nullable()->change();


            $table->string('ip')->nullable()->after('description');
            $table->text('browser_detail')->nullable()->after('ip');
            $table->text('request_detail')->nullable()->after('browser_detail');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('activity_log', function (Blueprint $table) {
            $table->integer('subject_id')->nullable()->change();
            $table->integer('causer_id')->nullable()->change();

            $table->dropColumn('ip');
            $table->dropColumn('browser_detail');
            $table->dropColumn('request_detail');
        });
    }
}
