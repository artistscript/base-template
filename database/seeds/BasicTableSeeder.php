<?php

use Illuminate\Database\Seeder;
use Facades\Helpers\PermissionGenerator;
use App\Models\User;

class BasicTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'email' => 'admin@digitalartisan.co.id',
            'password' => bcrypt('admin'),
            'name' => 'Administrator'
        ]);

        PermissionGenerator::generateAdminRole(1);
        PermissionGenerator::allowSidemenuForAdmin();

        $this->command->info('Admin User, Roles, and Basic Permission Set!');
    }
}
