<?php

use Illuminate\Database\Seeder;
use App\Models\MenuItem;

class SidemenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = database_path('base'.DIRECTORY_SEPARATOR.'menu.json');
        if (file_exists($path)) {
            MenuItem::insert(json_decode(file_get_contents($path), true));
        }
    }
}
